﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace lab2
{
    public class MatricesReadWrite
    {
        int sizeX;
        int sizeY;
        int[,] matrix;

        public int SizeX
        {
            get
            {
                return sizeX;
            }
        }

        public int SizeY
        {
            get
            {
                return sizeY;
            }
        }

        public int[,] Matrix
        {
            get
            {
                return matrix;
            }
        }

        public void ReadMatrixFromStreamreader(String path)
        {
            FileStream fsMatrix = new FileStream(path, FileMode.Open);
            StreamReader stream = new StreamReader(fsMatrix);
            String firstLine = stream.ReadLine();
            Regex re = new Regex(@"\d+");
            Match mt = re.Match(firstLine);
            String resultString = mt.Value;
            int sizeX = Int32.Parse(resultString);
            mt = mt.NextMatch();
            resultString = mt.Value;
            int sizeY = Int32.Parse(resultString);
            int[,] matrix = new int[sizeX, sizeY];
            for (int x = 0; x < sizeX; x++)
            {
                String arrayLine = stream.ReadLine();
                mt = re.Match(arrayLine);
                matrix[x, 0] = Int32.Parse(mt.Value);
                for (int y = 1; y < sizeY; y++)
                {
                    mt = mt.NextMatch();
                    matrix[x, y] = Int32.Parse(mt.Value);
                }
            }
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.matrix = matrix;
            stream.Close();
            fsMatrix.Close();
        }
        public void WriteMatrixtFile(int[,] matrix, String path)
        {      
            StreamWriter sw = new StreamWriter(path);
            sw.Write("[" + matrix.GetLength(0).ToString() + "\t" + matrix.GetLength(1).ToString() + "]");
            for (int x = 0; x < matrix.GetLength(0); x++)
            {
                sw.WriteLine();
                for (int y = 0; y < matrix.GetLength(1); y++)
                {
                    sw.Write(matrix[x, y].ToString() + "\t");
                }
            }
            sw.Close();

        }
    }
}