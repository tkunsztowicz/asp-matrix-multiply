﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace lab2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "Multiply matrices";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
            
            FileUpload1.SaveAs(@"C:\matrix1.txt");
            FileUpload2.SaveAs(@"C:\matrix2.txt");

            MatricesReadWrite obMatrix1 = new MatricesReadWrite();
            obMatrix1.ReadMatrixFromStreamreader(@"C:\matrix1.txt");
            int[,] matrix1 = obMatrix1.Matrix;


            MatricesReadWrite obMatrix2 = new MatricesReadWrite();
            obMatrix2.ReadMatrixFromStreamreader(@"C:\matrix2.txt");
            int[,] matrix2 = obMatrix2.Matrix;

            MatrixOperations operation = new MatrixOperations();
            int[,] matrix3 = operation.MatrixMultiply(matrix1, matrix2);

            MatricesReadWrite obMatrix3 = new MatricesReadWrite();
            obMatrix3.WriteMatrixtFile(matrix3, @"C:\matrix3.txt");

            Label1.Text = "calculation time: " + operation.CalcTime.ToString() + " ms";           

        }

        protected void FileUpload1_Load(object sender, EventArgs e)
        {
          
        }

        protected void FileUpload1_PreRender(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.ContentType = "Application /pdf";
            Response.AppendHeader("content-disposition",
                    "attachment; filename=" + "matrix3.txt");

            Response.TransmitFile(@"C:\matrix3.txt");
            Response.End();
        }
    }
}