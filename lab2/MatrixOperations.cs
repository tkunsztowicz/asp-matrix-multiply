﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace lab2
{
    public class MatrixOperations
    {
        long calcTime;

        public long CalcTime
        {
            get
            {
                return calcTime;
            }
        }
       
        public int[,] MatrixMultiply(int[,] matA, int[,] matB)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            
            int matACols = matA.GetLength(1);
            int matBCols = matB.GetLength(1);
            int matARows = matA.GetLength(0);
            int[,] result = new int[matBCols,matARows];
            // A basic matrix multiplication.
            // Parallelize the outer loop to partition the source array by rows.
            Parallel.For(0, matARows, i =>
            {
                for (int j = 0; j < matBCols; j++)
                {
                    int temp = 0;
                    for (int k = 0; k < matACols; k++)
                    {
                        temp += matA[i, k] * matB[k, j];
                    }
                    result[i, j] = temp;
                }
            }); // Parallel.For
            stopwatch.Stop();
            calcTime = stopwatch.ElapsedMilliseconds;
            return result;
        
        }
    }
}